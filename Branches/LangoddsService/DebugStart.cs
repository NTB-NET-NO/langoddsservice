﻿using System;
using System.Windows.Forms;
using LangoddsService;


namespace NTB.Langodds.Service
{
    public class DebugStart
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DebugForm());

        }
    }
}
