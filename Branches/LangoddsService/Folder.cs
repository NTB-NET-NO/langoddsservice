﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

namespace LangoddsService
{



    /// <summary>
    /// This class shall deal with folders and the different 
    /// jobs that are in these folders
    /// </summary>
    class Folder
    {
        



        /// <summary>
        /// This method gets the names in the root folder
        /// </summary>
        public Folder()
        {
            try
            {
                // Get the root directory for XSLT and XML folders
                string dirRootPath = ConfigurationManager.AppSettings["dirRootPath"];
                string dirOutPath = ConfigurationManager.AppSettings["dirOutputPath"];
                
                string dirInputPath = ConfigurationManager.AppSettings["dirInputPath"]; // Properties.Settings.Default.dirInputPath;
                string dirErrorPath = ConfigurationManager.AppSettings["dirErrorPath"]; // Properties.Settings.Default.dirErrorPath;
                string dirLogPath = ConfigurationManager.AppSettings["dirLogPath"]; // Properties.Settings.Default.dirLogPath;
                string dirDonePath = ConfigurationManager.AppSettings["dirDonePath"]; // Properties.Settings.Default.dirDonePath;
                
                // Check if they exists. If they don't, we shall create them
                CheckFolderExists(dirInputPath);
                CheckFolderExists(dirOutPath);
                CheckFolderExists(dirErrorPath);
                CheckFolderExists(dirRootPath);
                CheckFolderExists(dirLogPath);
                CheckFolderExists(dirDonePath);

            }
            catch (IOException iox)
            {
                Console.WriteLine(iox.Message.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }


        }

        /// <summary>
        /// This method is just a wrapper for the di.exists method
        /// </summary>
        /// <param name="folder">The folder we shall check if exists</param>
        public void CheckFolderExists(string folder)
        {
            DirectoryInfo di = new DirectoryInfo(folder);
            if (!di.Exists)
            {
                di.Create();
            }
        }

    }
}
