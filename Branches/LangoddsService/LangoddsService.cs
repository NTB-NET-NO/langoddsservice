﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using log4net;

using NTB.Langodds.Components;

namespace LangoddsService
{
    public partial class LangoddsService : ServiceBase
    {

        static ILog logger = LogManager.GetLogger(typeof(LangoddsService));

        /// <summary>
        /// The actual service object
        /// </summary>
        protected MainServiceComponent service = new MainServiceComponent();


        public LangoddsService()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            MDC.Set("JOBNAME", "LangoddsService");
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                logger.Info("LangoddsService starting");
                service.Configure();

                logger.Info("LangoddsService started");
            }
            catch (Exception ex)
            {
                logger.Fatal("LangoddsService DID NOT START properly", ex);
            }
        }

        protected override void OnStop()
        {
            try
            {
                service.Stop();
                logger.Info("LangoddsService stopped");
            }
            catch (Exception ex)
            {
                logger.Fatal("LangoddsService DID NOT STOP properly", ex);
            }
        }
    }
}
