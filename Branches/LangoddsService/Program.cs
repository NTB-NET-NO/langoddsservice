﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using log4net;

namespace LangoddsService
{
    public class Program
    {
        #region Logging
        /// <summary>
        /// Static logger
        /// </summary>
        static ILog _logger = LogManager.GetLogger(typeof(Program));

        #endregion


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new LangoddsService() 
			};
            ServiceBase.Run(ServicesToRun);
        }

    }
}
