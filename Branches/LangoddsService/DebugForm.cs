﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using log4net;

using NTB.Langodds.Components;

namespace LangoddsService
{
    public partial class DebugForm : Form
    {
        static ILog logger = null;

        protected MainServiceComponent Service = null;
        public DebugForm()
        {
            
            log4net.ThreadContext.Properties["JOBNAME"] = "LangoddsService-Debug";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(DebugForm));

            Service = new MainServiceComponent();

            InitializeComponent();
        }

        private void DebugForm_Load(object sender, EventArgs e)
        {
            try
            {
                logger.Info("LangoddsService DEBUGMODE starting...");
                this._startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                logger.Fatal("LangoddsService DEBUGMODE DID NOT START properly", ex);
            }
        }

        private void DebugForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Service.Stop();
                logger.Info("SportsDataService DEBUGMODE stopped");
            }
            catch (Exception ex)
            {
                logger.Fatal("SportsDataService DEBUGMODE DID NOT STOP properly", ex);
            }
        }

        private void _startupConfigTimer_Tick(object sender, EventArgs e)
        {
            //Kill the timer
            this._startupConfigTimer.Stop();

            //And configure
            try
            {
                Service.Configure();   
                Service.Start();
                logger.Info("SportsDataService DEBUGMODE started");
            }
            catch (Exception ex)
            {
                logger.Fatal("SportsDataService DEBUGMODE DID NOT START properly", ex);
            }
        }
    }
}