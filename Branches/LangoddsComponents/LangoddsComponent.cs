﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

// Adding support for log4net
using NTB.Langodds.Components;
using NTB.Langodds.Utilities;
using log4net;
using log4net.Config;

// using NTB.NewsFeed.Utilities;
using System.Threading;
using System.Configuration;

namespace NTB.Langodds.Components

{
    public partial class LangoddsComponent : Component, IBaseServiceComponent
    {
        /// <summary>
        /// Static logger
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(MainServiceComponent));

        #region Common class variables
        /// <summary>
        /// Busy status event
        /// </summary>
        private System.Threading.AutoResetEvent _busyEvent = new System.Threading.AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private System.Threading.AutoResetEvent _stopEvent = new System.Threading.AutoResetEvent(false);

        /// <summary>
        /// The configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected String name;

        /// <summary>
        /// The enabled status for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="Enabled"/></remarks>
        protected Boolean enabled;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly configured
        /// </summary>
        /// <remarks>Holds the configured status of the instance. <c>True</c> means successfully configured</remarks>
        protected Boolean configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected Boolean errorRetry = false;

        /// <summary>
        /// PollDelay is used to make sure that the file is ready to be accessed. 
        /// </summary>
        protected int PollDelay = 0;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected String emailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected String emailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected String emailBody;

        /// <summary>
        /// Flag to tell which state the component is in (running/halted)
        /// </summary>
        protected ComponentState componentState;

        #endregion

        #region Polling settings

        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;

        /// <summary>
        /// The configured schedule for this instance
        /// </summary>
        /// <remarks>This is set to a time of day and is used by the <c>Scheduled</c> pollstyle. Work is triggered at the given time of day.</remarks>
        protected String schedule;

        /// <summary>
        /// The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int interval = 60;



        #endregion

        #region File folders

        /// <summary>
        /// Input file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="fileFilter"/> and <see cref="includeSubdirs"/>
        /// </remarks>
        protected String fileInputFolder;

        /// <summary>
        /// Output file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="fileFilter"/> and <see cref="includeSubdirs"/>
        /// </remarks>
        protected Dictionary<int, string> fileOutputFolders = new Dictionary<int, string>();

        protected String fileXSLTFolder;
        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected String fileErrorFolder;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected String fileDoneFolder;

        #endregion

        #region Optional configuration

        /// <summary>
        /// Input file filter
        /// </summary>
        /// <remarks>
        ///   <para>Defines what file types are being read from <see cref="fileInputFolder"/>.</para>
        ///   <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected String fileFilter = "*.txt";

        /// <summary>
        /// Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///   <para>Defines if the instance should look in subdirectories within <see cref="fileInputFolder"/>.</para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected Boolean includeSubdirs = false;

        /// <summary>
        /// Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///   <para>Defines the internal buffer size the FileSystemWatcher when using this polling.
        /// Should not be set to high. The instance will fall back to contious polling on buffer overflows.</para>
        ///   <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int bufferSize = 4096;

        /// <summary>
        /// Flag to say if this component is working or not. I have a feeling we have another parameter for this though.
        /// </summary>
        protected bool _BusyFlag = false;

        /// <summary>
        /// Dictionary to map ID to encodings, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> documentEncodings = new Dictionary<int, string>();

        /// <summary>
        /// Dictionary to map ID to file extensions, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> documentExtensions = new Dictionary<int, string>();


        #endregion

        public LangoddsComponent()
        {
            InitializeComponent();
        }

        public LangoddsComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void Configure(XmlNode configNode)
        {
            this.pollTimer.Enabled = false;
            logger.Debug("Node: " + configNode.Name);
            logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));

            //Basic configuration sanity check
            if (configNode == null ||
                configNode.Name != "LangoddsComponent" ||
                configNode.Attributes.GetNamedItem("Name") == null)
            {
                throw new ArgumentException("The XML configuration node passed is invalid",
                    "configNode");
            }



            #region Basic config

            // Getting the name of this Component instance
            try
            {
                name = configNode.Attributes["Name"].Value;
                fileFilter = configNode.Attributes["FileFilter"].Value;
                enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                log4net.ThreadContext.Stacks["NDC"].Push(InstanceName);
                
            }
            catch (Exception ex)
            {
                logger.Fatal("Not possible to configure this job instance!", ex);
            }

            //Basic operation
            try
            {
                pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }

            try
            {
                OutputPrefix = configNode.Attributes["OutputPrefix"].Value;
            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OutputPrefix values in XML configuration node", ex);
            }

            try
            {
                OutputFiletype = configNode.Attributes["OutputFiletype"].Value;
            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OutputFiletype values in XML configuration node", ex);
            }


            #endregion

            #region Folder generation
            try
            {
                //Find the file folder to work with

                XmlNode FolderNode = configNode.SelectSingleNode("InputFolder");
                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");

                if (FolderNode != null)
                {
                    InputFolder = FolderNode.InnerText;


                    if (!Directory.Exists(InputFolder))
                    {
                        Directory.CreateDirectory(InputFolder);
                    }

                }

                logger.InfoFormat("Converter job - Polling: {0} / XMLFeed Folder: {1} / Enabled: {2}",
                        Enum.GetName(typeof(PollStyle), pollStyle), InputFolder, enabled);
            }
            catch (Exception ex)
            {
                logger.Fatal("Not possible to get XMLFeed Folder", ex);
            }

            try
            {
                //Find the file folder to work with

                XmlNode FolderNode = configNode.SelectSingleNode("OutputFolder");
                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");

                if (FolderNode != null)
                {
                    OutputFolder = FolderNode.InnerText;

                    string OutputFolderFilePath = Path.GetDirectoryName(OutputFolder);

                    if (!Directory.Exists(OutputFolderFilePath))
                    {
                        Directory.CreateDirectory(OutputFolderFilePath);
                    }

                }

                logger.InfoFormat("Converter job - Polling: {0} / OutputFolder : {1} / Enabled: {2}",
                        Enum.GetName(typeof(PollStyle), pollStyle), OutputFolder, enabled);
            }
            catch (Exception ex)
            {
                logger.Fatal("Not possible to get Error-folder", ex);
            }



            try
            {
                //Find the file folder to work with

                XmlNode FolderNode = configNode.SelectSingleNode("ErrorFolder");
                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (FolderNode != null)
                {
                    ErrorFolder = FolderNode.InnerText;

                    if (!Directory.Exists(ErrorFolder))
                    {
                        Directory.CreateDirectory(ErrorFolder);
                    }

                }

                logger.InfoFormat("Converter job - Polling: {0} / Directory Error folder: {1} / Enabled: {2}",
                        Enum.GetName(typeof(PollStyle), pollStyle), ErrorFolder, enabled);
            }
            catch (Exception ex)
            {
                logger.Fatal("Not possible to get Error-folder", ex);
            }

            try
            {
                //Find the file folder to work with

                XmlNode FolderNode = configNode.SelectSingleNode("DoneFolder");
                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (FolderNode != null)
                {
                    DoneFolder = FolderNode.InnerText;

                    if (!Directory.Exists(DoneFolder))
                    {
                        Directory.CreateDirectory(DoneFolder);
                    }

                }

                logger.InfoFormat("Converter job - Polling: {0} / Directory Done folder: {1} / Enabled: {2}",
                        Enum.GetName(typeof(PollStyle), pollStyle), DoneFolder, enabled);
            }
            catch (Exception ex)
            {
                logger.Fatal("Not possible to get Directory Cache-folder", ex);
            }


            try
            {
                //Find the file folder to work with

                XmlNode FolderNode = configNode.SelectSingleNode("InputFolder");
                
                if (FolderNode != null)
                {
                    fileInputFolder = FolderNode.InnerText;

                }

                logger.InfoFormat("Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                        Enum.GetName(typeof(PollStyle), pollStyle), fileInputFolder, enabled);
            }
            catch (Exception ex)
            {
                logger.Fatal("Not possible to get Input-folder", ex);
            }

            #endregion
            // This is where the FileFolder creation shall end

            if (enabled)
            {
                #region File folders to access
                // Checking if file folders exists
                string fileOutputFolder = "";

                try
                {
                    if (!Directory.Exists(fileInputFolder))
                    {
                        Directory.CreateDirectory(fileInputFolder);
                    }

                    foreach (KeyValuePair<int, string> kvp in fileOutputFolders)
                    {
                        fileOutputFolder = kvp.Value;
                        if (!Directory.Exists(fileOutputFolder))
                        {
                            Directory.CreateDirectory(fileOutputFolder);
                        }
                    }

                }
                catch (Exception ex)
                {
                    log4net.ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + fileOutputFolder, ex);
                }

                #endregion



                #region Set up polling
                try
                {
                    //Switch on pollstyle
                    switch (pollStyle)
                    {
                        case PollStyle.Continous:
                            interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                            if (interval == 0)
                            {
                                interval = 5000;
                            }
                            pollTimer.Interval = interval; // short startup - interval * 1000;
                            logger.DebugFormat("Poll interval: {0} seconds", interval);

                            break;



                        case PollStyle.Scheduled:
                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");


                        case PollStyle.FileSystemWatch:
                            //Check for config overrides

                            if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                                bufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                            logger.DebugFormat("FileSystemWatcher buffer size: {0}", bufferSize);

                            // Set values
                            fileWatcher.Path = fileInputFolder;
                            fileWatcher.Filter = fileFilter;
                            fileWatcher.IncludeSubdirectories = includeSubdirs;
                            fileWatcher.InternalBufferSize = bufferSize;

                            //Do not start the event watcher here, wait until after the startup cleaning job
                            pollTimer.Interval = 5000; // short startup;

                            break;

                        default:
                            //Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type");
                    }
                }
                catch (Exception ex)
                {
                    log4net.ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }
                #endregion
            }
            //Finish configuration
            log4net.ThreadContext.Stacks["NDC"].Pop();
            configured = true;

        }

        protected string DoneFolder { get; set; }

        protected string ErrorFolder { get; set; }

        protected string OutputFolder { get; set; }

        protected string InputFolder { get; set; }

        protected string OutputFiletype { get; set; }

        protected string OutputPrefix { get; set; }
        

        public void Start()
        {
            if (!configured)
            {
                throw new LangoddsException("LangoddsComponent is not properly configured. LangoddsComponent::Start() Aborted!");
            }

            if (!enabled)
            {
                throw new LangoddsException("LangoddsComponent is not enabled. LangoddsComponent::Start() Aborted!");
            }

            logger.Info("In Start() " + this.InstanceName + ".");

            // I first want to check input folder and see if there are files in there. If there are, they are to be converted
            DirectoryInfo di = new DirectoryInfo(fileInputFolder);
            FileInfo[] diFiles = di.GetFiles();

            // Clean up old files

            List<FileInfo> Files = di.GetFiles().OrderByDescending(f => f.CreationTime).ToList();

            foreach (FileInfo myFile in Files)
            {
                LangoddsConverter myConverter = new LangoddsConverter
                    {
                        DonePath = this.DoneFolder,
                        ErrorPath = this.ErrorFolder,
                        InputPath = this.InputFolder,
                        OutPath = this.OutputFolder
                    };
                myConverter.GenerateLangodds(myFile.Name);
            }
            
            _stopEvent.Reset();
            _busyEvent.Set();



            componentState = ComponentState.Running;
            pollTimer.Start();

        }

        public void Stop()
        {
            if (!configured)
            {
                throw new LangoddsException("LangoddsComponent is not properly configured. LangoddsComponent::Stop() Aborted");
            }

            if (!enabled)
            {
                throw new LangoddsException("LangoddsComponent is not properly configured. LangoddsComponent::Stop() Aborted");
            }




            if (!pollTimer.Enabled && (errorRetry || pollStyle !=
                Components.PollStyle.FileSystemWatch || pollTimer.Interval == 5000))
            {
                logger.InfoFormat("Waiting for instance to complete work. Job {0}", InstanceName);
            }

            // Signal Stop
            _stopEvent.Set();

            // Stop events
            fileWatcher.EnableRaisingEvents = false;

            if (!_busyEvent.WaitOne(30000))
            {
                logger.InfoFormat("Instance did not complete properly. Data may be inconsistent. Job: {0}", InstanceName);
            }

            componentState = ComponentState.Halted;
            // Kill polling
            pollTimer.Stop();


        }

        public bool Enabled
        {
            get { return enabled; }
        }

        public string InstanceName
        {
            get { return name; }
        }

        public OperationMode OperationMode
        {
            get { return OperationMode.Converter; }
        }

        public PollStyle PollStyle
        {
            get { return pollStyle; }
        }

        public ComponentState ComponentState
        {
            get { return componentState; }
        }

        private void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            log4net.ThreadContext.Properties["JOBNAME"] = InstanceName;
            logger.DebugFormat("LangoddsComponent::pollTimer_Elapsed() hit");

            //Reset flags and pollers
            _busyEvent.WaitOne();
            pollTimer.Stop();

            try
            {
                logger.Debug("InputFolder for " + InstanceName + " : " + fileInputFolder);
                //Process any wating files
                List<String> files = new List<string>(Directory.GetFiles(fileInputFolder,
                    fileFilter, (includeSubdirs ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)));

                //Some logging
                logger.DebugFormat("LangoddsComponent::pollTimer_Elapsed() Convertion items scheduled: {0}", files.Count);

                // I need to have an Debug-flag here
                if (ConfigurationManager.AppSettings["Debug"].ToUpper() == "TRUE")
                {
                    foreach (string stringFile in files)
                    {
                        logger.Debug("Files to be converted are: " + stringFile);

                        // Process the files
                        LangoddsConverter converter = new LangoddsConverter {DonePath = this.DoneFolder, 
                            InputPath = this.InputFolder, 
                            ErrorPath = this.ErrorFolder, 
                            OutPath = this.OutputFolder};

                        converter.GenerateLangodds(stringFile);
                    }
                }


                
                // Enable the event listening
                if (pollStyle == PollStyle.FileSystemWatch && !errorRetry)
                {
                    logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    fileWatcher.EnableRaisingEvents = true;
                }
                if (pollStyle == PollStyle.Continous && !errorRetry)
                {
                    pollTimer.Start();
                }
            }
            catch (Exception ex)
            {
                logger.Error("LangoddsComponent::pollTimer_Elapsed() error: " + ex.Message, ex);
            }


            //Check error state
            if (errorRetry)
            {
                fileWatcher.EnableRaisingEvents = false;
            }

            //Restart
            pollTimer.Interval = interval * 1000;
            if (pollStyle != PollStyle.FileSystemWatch || (pollStyle == PollStyle.FileSystemWatch && fileWatcher.EnableRaisingEvents == false))
                pollTimer.Start();

            _busyEvent.Set();
        }

        private void fileWatcher_Created(object sender, FileSystemEventArgs e)
        {
            try
            {
                log4net.ThreadContext.Properties["JOBNAME"] = InstanceName;
                logger.InfoFormat("LangoddsComponent::fileWatcher_Created() hit");

                //Wait for the busy state
                _busyEvent.WaitOne();


                List<string> files = new List<string>();
                files.Add(e.FullPath);


                foreach (string myFile in files)
                {
                    LangoddsConverter converter = new LangoddsConverter
                        {
                            DonePath = this.DoneFolder,
                            ErrorPath = this.ErrorFolder,
                            InputPath = this.InputFolder,
                            OutPath = this.OutputFolder
                        };
                    converter.GenerateLangodds(myFile);
                }

                //Reset event
                _busyEvent.Set();
            }
            catch (Exception exception)
            {
                logger.Error("Something happened during fileWatcher_Created!");
                logger.Error(exception.Message);
                logger.Error(exception.StackTrace.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strFolder"></param>
        //public void LoopDirectories(string strFolder)
        //{

        //    // Creating the string Path
        //    string strPath = "";
        //    if (!strFolder.StartsWith("c:"))
        //    {
        //        strPath = Properties.Settings.Default.dirInputPath + @"\" + strFolder;
        //    }
        //    else
        //    {
        //        strPath = strFolder;
        //    }
        //    StrMessage = "Dette er strPath: " + strPath;
        //    _logger.Info(StrMessage);
        //    // Get the folder

        //    LoopFiles(strPath);

        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strPath"></param>
        //public void LoopDirectory(string strPath)
        //{
        //    // Looping through the directory

        //    string[] directories = Directory.GetDirectories(strPath);
        //    foreach (string strDir in directories)
        //    {
        //        string strDirectory = strDir;
        //        _logger.Info(strDirectory);

        //        // DoConvert(strDir);
        //        // GenerateLangodds(string inputname)
        //        LoopDirectory(strDir);
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDirectory"></param>
        //public void LoopFiles(string strDirectory)
        //{
        //    string[] directory = Directory.GetFiles(strDirectory);

        //    foreach (string strFiles in directory)
        //    {

        //        logger.Info("Converting: " + strFiles);
        //        CreateDocument cd = new CreateDocument();
        //        cd.GenerateLangodds(strFiles);
        //    }
        //}
    }
}
