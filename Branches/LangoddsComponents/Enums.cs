﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Langodds.Components
{

    public enum ComponentState
    {
        /// <summary>
        /// This value is just used to tell if the component is in started modus (running)
        /// </summary>
        Running,

        /// <summary>
        /// this value is just used to tell if the component is in halted mode
        /// </summary>
        Halted
    }

    /// <summary>
    /// Used to specify the different operating modes. Primarily used for the common ThreadService poller component.
    /// In this template we've added some for reference. You can change these as your service applies. 
    /// </summary>
    public enum OperationMode
    {
        /// <summary>
        /// Normal Distributor mode, messages are extracted, converted to NITF and saved to disk
        /// </summary>
        Distributor,

        /// <summary>
        /// Downloader mode collects data from different online sources and saves these to disk
        /// </summary>
        Downloader,

        /// <summary>
        /// Deletes items older than the specified time to live
        /// </summary>
        Purge,

        /// <summary>
        /// Converter mode collects XML files from disk, converts and stores them in out folders 
        /// </summary>
        Converter,

        /// <summary>
        /// Scheduler mode does work only at scheduled times. 
        /// </summary>
        Scheduler

    }

    /// <summary>
    /// Specifies the different polling styles available to the EWS poller component
    /// </summary>
    public enum PollStyle
    {
        /// <summary>
        /// Indicates continous polling og an item at a timed interval.
        /// This is the legacy pollingstyle used for 
        /// </summary>
        Continous,
        /// <summary>
        /// Polling is limited to a time schedule, i.e. once a day/week
        /// </summary>
        Scheduled,
        /// <summary>
        /// Contious polling using an EWS folder subscription set.
        /// Allows for easier Exchange folder item checking.
        /// </summary>
        PullSubscribe,
        /// <summary>
        /// Notifications from EWS are actively sent to the client using a web service.
        /// </summary>
        PushSubscribe,
        /// <summary>
        /// Notifications for jobs working with local disk folders, i.e. Gatherer-jobs
        /// </summary>
        FileSystemWatch
    }
}