﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;
using System.Xml;
using System.Configuration;
using log4net;

namespace LangoddsService
{
    public class CreateDocument
    {

        private string dirOutPath = ConfigurationManager.AppSettings["dirOutPath"];
        private string dirInputPath = ConfigurationManager.AppSettings["dirInputPath"];
        private string dirErrorPath = ConfigurationManager.AppSettings["dirErrorPath"]; 
        private string dirLogPath = ConfigurationManager.AppSettings["dirLogPath"]; 
        private string dirDonePath = ConfigurationManager.AppSettings["dirDonePath"]; 

        private string strMessage = "";

        #region Logging
        /// <summary>
        /// Static logger
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(CreateDocument));

        #endregion

        public CreateDocument()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!log4net.LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        

        

        public void GenerateLangodds(string inputname)
        {
            try
            {

                // Creating a StreamReader
                strMessage = "Reading: " + inputname;
                logger.Info(strMessage);
                StreamReader inn = new StreamReader(inputname, Encoding.GetEncoding("iso-8859-1"));

                FileInfo fi = new FileInfo(inputname);
                string filename = fi.Name;
                

                // Creating a DateTime which uses the last write time to the file
                DateTime dt = File.GetLastWriteTime(inputname);

                // Get the current Culture
                CultureInfo ciCurr = CultureInfo.CurrentCulture;

                DateTime dtPassed = DateTime.Now;

                int weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                string weekDay = ciCurr.Calendar.GetDayOfWeek(dtPassed).ToString();

                // Creating a stringarray for the different parts in the file
                string[] parts;

                // This is the content of the line
                string l = "";


                // Creating a StringBuilder string to hold the content of the file before writing it
                StringBuilder strFileContent = new StringBuilder();

                string outfile = "";
                outfile = Path.Combine(dirOutPath, Path.GetFileName(inputname).Replace(".txt", ".xml"));

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.IndentChars = ("\t");
            

                using (XmlWriter writer = XmlWriter.Create(outfile, settings))
                {
                    
                    strMessage = "Creating " + outfile;

                    logger.Info(strMessage);




                    // Creating the start of the XML-file
                    writer.WriteStartDocument();
                    writer.WriteStartElement("langodds");
                    writer.WriteElementString("week", weekNum.ToString());
                    
                    

                    //strFileContent.AppendLine(@"<langodds>");
                    //strFileContent.AppendLine(@"<week>" + weekNum + @"</week>");
                    //strFileContent.AppendLine(@"<weekdays>");
                    //strFileContent.AppendLine(@"<weekday>" + weekDay + @"</weekday>");

                    string strStoreTime = "";
                    string strStorePlayDay = "";
                    bool GamesTag = false;
                    bool WeekTag = false;
                    string str59er = "";

                    string strDate = "";
                    string strDay = "";

                    string strStoreDay = "";

                    while (inn.Peek() >= 0)
                    {
                        // Reads the content of the current line
                        l = inn.ReadLine();
                        strMessage = "Computing following line: " + l;
                        logger.Info(strMessage);
                        // Splitting the line with tabs
                        parts = Regex.Split(l, "\t");

                        // Cleaning up the different posts in the array
                        // Removing spaces with trim
                        int intParts = parts.Length;
                        strMessage = "This line contains " + parts.Length.ToString() + " parts";
                        logger.Info(strMessage);

                        int intCounter = 0;
                        foreach (string part in parts)
                        {
                            string tmp = part;
                            parts[intCounter] = tmp.Trim();
                            intCounter++;
                        }

                        intParts = parts.Length;
                        strMessage = "After trimming this line contains " + parts.Length.ToString() + " parts";
                        logger.Info(strMessage);

                        // Checking if the split results in less than 12 posts
                        if (parts.Length < 13)
                        {

                            // Doing some initial checks
                            if (parts.Length < 8)
                            {
                                strMessage = "We are now adding tabulators to get to eight parts";
                                logger.Info(strMessage);

                                strMessage = "This line contains: " + parts.Length + " parts!";
                                logger.Info(strMessage);
                                int intEightParts = 8;
                                int intPartsLeft = intEightParts - parts.Length - 1;
                                string strPartsAdding = "";

                                while (intPartsLeft < intEightParts)
                                {
                                    strPartsAdding += "\t";
                                    intPartsLeft++;
                                }



                                string temp = parts[parts.Length-1].ToString();

                                // adding a tabulator to this line
                                parts[parts.Length - 1] = temp + strPartsAdding;

                                string newline = String.Join("\t", parts);

                                strMessage = "This string is now like this: " + newline;
                                logger.Info(strMessage);

                                // This splits the line.
                                parts = Regex.Split(newline, "\t");

                                strMessage = "After checking for line 8 this line NOW contains " + parts.Length.ToString() + " parts";
                                logger.Info(strMessage);
                            }

                            // Checking if parts[8] starts with R
                            strMessage = "We are checking parts[8]: '" + parts[8] + "'";
                            logger.Info(strMessage);

                            if (parts[8].StartsWith("R"))
                            {
                                strMessage = "We are going to add a tab in front of the R";
                                logger.Info(strMessage);

                                string temp = parts[8].ToString();
                                parts[8] = "\t" + temp;

                                string newline = String.Join("\t", parts);

                                strMessage = "This string is now like this: " + newline;
                                logger.Info(strMessage);

                                parts = Regex.Split(newline, "\t");

                                strMessage = "After checking for a R-line, this line NOW contains " + parts.Length.ToString() + " parts";
                                logger.Info(strMessage);
                            }



                            // Checking if we now have 13 array posts
                            if (parts.Length < 13)
                            {

                                int intAddingTabs = 0;
                                int intMissingTabs = 13 - parts.Length;

                                while (intAddingTabs < intMissingTabs)
                                {
                                    parts[8] += "\t";
                                    intAddingTabs++;
                                }

                                string newline = String.Join("\t", parts);

                                parts = Regex.Split(newline, "\t");

                                strMessage = "After adding " + intMissingTabs.ToString() + " this line now contains " + parts.Length.ToString() + " parts";
                                logger.Info(strMessage);


                                // if it's still less than 13 we'll add tabs at the end
                                if (parts.Length < 13)
                                {
                                    // Setting max and min
                                    int max = 12;
                                    int min = parts.Length;
                                    int debugmin = max - min;
                                    string strTabs = "";

                                    // Looping until min eq max
                                    while (min <= max)
                                    {
                                        // Add tabs to the strTabs string
                                        strTabs += "\t";
                                        min++;
                                    }

                                    // Add tabs to the end of the newline string
                                    newline += strTabs;

                                    // Split line based on tabs. Now we shall have 12/13 arrays
                                    parts = Regex.Split(newline, "\t");

                                    strMessage = "After adding " + debugmin + " this line NOW contains " + parts.Length.ToString() + " parts";
                                    logger.Info(strMessage);

                                }
                            }
                        }
                        strMessage = "Done cleaning parts";
                        logger.Info(strMessage);

                        /**
                         * 
                         * Since we now know the structure, 
                         * why not just put the different pieces into the strings
                         * 1: Dag(+dato)
                         * 2: Klokkeslett
                         * 3: Kampnummer (int)
                         * 4: Kamp (string)
                         * 5: Informasjon om kamp på nøytralbane
                         * 6: S eller ingenting
                         * 7: Hjemmeodds, alltid tall eller ingenting
                         * 8: Tall eller tekst
                         * 9: Borte eller tall - blir borte dersom 8 er tekst
                         * 10: Ingenting eller R + tall
                         * 11: Blank eller kort tekst. Type spillobjekt
                         * 12: Liga/spill-informasjon (FIS, EL osv)
                         * 13: TV(-kanal)
                         * 
                         */

                        
                        strDay = parts[0];
                        
                        
                        
                        string strTime = parts[1];
                        string strMatchNumber = parts[2];
                        string strMatch = parts[3];
                        /* 
                         * Removing & if we have such a character, 
                         * but only if we have it between spaces. 
                         * This so that we don't get:
                         * &amp; -> &amp;amp;amp;.....
                         */
                        strMatch = strMatch.Replace(" & ", " &amp; ");
                        string strNeutralField = parts[4];
                        string strSField = parts[5];
                        string strHomeOdds = parts[6];
                        string strEvenOdds = parts[7];
                        string strAwayOdds = parts[8];
                        string strRNumber = parts[9];
                        string strGameObject = parts[10];
                        string strLeagueObject = parts[11];
                        string strTVChannel = parts[12];


                        if (parts[0] != "")
                        {
                            string tmp = parts[0];
                            

                            switch (tmp.Substring(0,2))
                            {
                                case "Sø":
                                    strDay = "Søndag";
                                    break;
                                case "Ma":
                                    strDay = "Mandag";
                                    break;
                                case "Ti":
                                    strDay = "Tirsdag";
                                    break;
                                case "On":
                                    strDay = "Onsdag";
                                    break;
                                case "To":
                                    strDay = "Torsdag";
                                    break;
                                case "Fr":
                                    strDay = "Fredag";
                                    break;
                                case "Lø":
                                    strDay = "Lørdag";
                                    break;
                            }

                            


                            
                            tmp = tmp.Insert(tmp.Length - 2, @"/");
                            strDate = tmp.Substring(2).Trim().ToString();
                            // strStorePlayDay = tmp;
                        }

                        
                        

                        // Get the Play day
                        if (strStorePlayDay == "")
                        {
                            strStorePlayDay = parts[0];
                        }
                        else
                        {
                            if (parts[0] != "")
                            {
                                if ((parts[0] != strStorePlayDay) && (strStorePlayDay != ""))
                                {
                                    strStorePlayDay = parts[0];
                                }
                                else if ((parts[0] == strStorePlayDay) && (strStorePlayDay != ""))
                                {
                                    parts[0] = "";
                                }
                            }
                        }

                        strStoreDay = strDay;

                        if (strStoreTime == "")
                        {
                            strStoreTime = parts[1];
                        }
                        else
                        {
                            if (parts[1] != "")
                            {
                                if ((parts[1] != "") && (strStoreTime != ""))
                                {
                                    strStoreTime = parts[1];
                                }
                                else if ((parts[1] == strStoreTime) && (strStoreTime != ""))
                                {
                                    parts[1] = "";
                                }
                            }
                        }
                        // Get the time

                        if ((parts[1] != "") || (strStoreTime != ""))
                        {
                            string tmp = parts[1];

                            // Removing kl, since we are creating an XML-file
                            tmp = tmp.Replace("kl", "");

                            // Inserting a dot so that we get 17.25, only if we have 1725
                            if (tmp.IndexOf(".") == 0)
                            {
                                tmp = tmp.Insert(tmp.Length - 2, ".");
                            }

                            // Just in case we get 17..25, we are removing one dot!
                            tmp = tmp.Replace("..", ".");

                            strStoreTime = tmp;
                        }

                        if (str59er == "")
                        {
                            str59er = strStorePlayDay;
                        }
                        else
                        {
                            // Check if content of str59er is the same as parts[0]. 
                            // Set all things related to dates to ""
                            if (str59er == strStorePlayDay)
                            {
                                // But only if we are on line 59
                                if (strMatchNumber == "60")
                                {
                                    strDay = "";
                                    strDate = "";
                                    strStoreTime = "";
                                }
                            }
                            if (str59er != parts[0])
                            {
                                str59er = parts[0];
                            }
                        }

                        


                        if (strDay != "")
                        {
                            if (WeekTag == true)
                            {
                                writer.WriteEndElement(); // Ending games-tag
                                writer.WriteEndElement(); // Ending weekday tag
                                WeekTag = false;
                                GamesTag = false;
                            }
                            writer.WriteStartElement("weekdays");
                            writer.WriteElementString("weekday", strDay);
                            writer.WriteElementString("date", strDate);
                            WeekTag = true;
                        }

                        

                        if (strStoreTime != "")
                        {
                            if (GamesTag == true)
                            {
                                writer.WriteEndElement();
                                GamesTag = false;

                                
                            }
                            writer.WriteStartElement("games");
                            writer.WriteStartAttribute("time");
                            writer.WriteString(strTime);
                            writer.WriteStartAttribute("date");
                            writer.WriteString(strDate);
                            writer.WriteStartAttribute("day");
                            writer.WriteString(strStoreDay);
                            GamesTag = true;
                            // writer.writes.AppendLine(@"<games time=\""" + strStoreTime + "\">\r\n");
                        }

                        


                        strMessage = "About to create strFileContent";
                        logger.Info(strMessage);

                        writer.WriteStartElement("game");

                        // Creating game number 
                        writer.WriteElementString("gamenumber", strMatchNumber);
                        writer.WriteElementString("gameteams", strMatch);
                        writer.WriteElementString("gameneutral", strNeutralField);
                        writer.WriteElementString("gamesfield", strSField);
                        writer.WriteElementString("gamehomeodds", strHomeOdds);
                        writer.WriteElementString("gameevenodds", strEvenOdds);
                        writer.WriteElementString("gameawayodds", strAwayOdds);
                        writer.WriteElementString("rnumber", strRNumber);
                        writer.WriteElementString("gameobject", strGameObject);
                        writer.WriteElementString("leagueobject", strLeagueObject);
                        writer.WriteElementString("tvchannel", strTVChannel);
                        writer.WriteEndElement(); // Ending Game
                        // writer.WriteEndElement(); // Ending Games
                        //strFileContent.AppendLine("<game>");
                        //strFileContent.AppendLine("<gamenumber>" + strMatchNumber + "</gamenumber>");
                        //strFileContent.AppendLine("<gameteams>" + strMatch + "</gameteams>");
                        //strFileContent.AppendLine("<gameneutral>" + strNeutralField + "</gameneutral>");
                        //strFileContent.AppendLine("<gamesfield>" + strSField + "</gamesfield>");
                        //strFileContent.AppendLine("<gamehomeodds>" + strHomeOdds + "</gamehomeodds>");
                        //strFileContent.AppendLine("<gameevenodds>" + strEvenOdds + "</gameevenodds>");
                        //strFileContent.AppendLine("<gameawayodds>" + strAwayOdds + "</gameawayodds>");
                        //strFileContent.AppendLine("<rnumber>" + strRNumber + "</rnumber>");
                        //strFileContent.AppendLine("<gameobject>" + strGameObject + "</gameobject>");
                        //strFileContent.AppendLine("<leagueobject>" + strLeagueObject + "</leagueobject>");
                        //strFileContent.AppendLine("<tvchannel>" + strTVChannel + "</tvchannel>");
                        //strFileContent.AppendLine("</game>");
                        //strFileContent.AppendLine("</games>");


                        // checking if the string is longer than three, meaning that 
                        // NT can have a string that is up to 999
                        //if (parts[2].Length > 3)
                        //{
                        //    int cleanup = 3;
                        //    if (parts[3] == strStoreTime)
                        //    {
                        //        cleanup = 4;
                        //    }

                        //    while (cleanup < parts.Length - 1)
                        //    {

                        //    }
                        //}

                    }

                    writer.WriteEndElement(); // Ending langodds
                    //strFileContent.AppendLine("</weekdays>");
                    //strFileContent.AppendLine("</langodds>");

                    


                    // Closing the readfile
                    inn.Dispose();


                    // Creating the filename for the file. Removing the .txt-extention and replacing it with .xml 

                    writer.WriteEndDocument();
                    writer.Flush();

                    writer.Close();

                }


                    strMessage = "--------------------------------------------------------";
                    logger.Info(strMessage);

                    strMessage = "moving file to " + dirDonePath + @"\" + filename;
                    logger.Info(strMessage);


                    FileInfo fif = new FileInfo(dirDonePath + @"\" + filename);
                    if (fif.Exists)
                    {
                        fif.Delete();
                        fi.MoveTo(dirDonePath + @"\" + filename);
                    } else {

                        fi.MoveTo(dirDonePath + @"\" + filename);
                    }





            }
            catch (IOException iox)
            {


                strMessage = iox.Message.ToString();
                logger.Info(strMessage);

                strMessage = iox.StackTrace.ToString();
                logger.Info(strMessage);


            }
            catch (IndexOutOfRangeException iex)
            {
                strMessage = iex.Message.ToString();
                logger.Info(strMessage);

                strMessage = iex.StackTrace.ToString();
                logger.Info(strMessage);
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();
                logger.Info(strMessage);

                strMessage = ex.StackTrace.ToString();
                logger.Info(strMessage);
            }
        }
    }
}