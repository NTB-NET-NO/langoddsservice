﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NTB.Langodds.Components;
using log4net;
using System.ServiceModel;
using System.Xml;
using System.Configuration;
using System.Threading;

namespace NTB.Langodds.Components
{
    public partial class MainServiceComponent : Component
    {
        static MainServiceComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!log4net.LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();

            logger = LogManager.GetLogger(typeof(MainServiceComponent));

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "MainServiceComponent";
            }
        }


        public MainServiceComponent()
        {
            InitializeComponent();
        }

        public MainServiceComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }



        /// <summary>
        /// Static logger
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(Langodds.Components.MainServiceComponent));

        /// <summary>
        /// Initializes the <see cref="Langodds.Components.MainServiceComponent"/> class.
        /// </summary>


        #region Instance variables and control data

        /// <summary>
        /// Name of the file that contains the job configuration set
        /// </summary>
        /// <remarks>The name of the job config XML file is stored here.</remarks>
        protected String configSet = String.Empty;

        /// <summary>
        /// Enable watching the config set for changes and reconfigure at runtime
        /// </summary>
        /// <remarks>config setting that defines if we are wathcing the config set for changes/auto reconfigure or not.</remarks>
        protected Boolean watchConfigSet = false;

        /// <summary>
        /// List of currently configured jobs
        /// </summary>
        protected Dictionary<String, IBaseServiceComponent> jobInstances = new Dictionary<String, IBaseServiceComponent>();

        /// <summary>
        /// Push subscription job dictionary, maps exchange folder ids to job names
        /// </summary>
        protected Dictionary<String, String> pushSubscriptionJobs = new Dictionary<String, String>();


        /// <summary>
        /// List of folders to match events against
        /// </summary>
        protected List<String> pushSubscribeFolderIds = new List<String>();

        /// <summary>
        /// Notification handler service host
        /// </summary>
        protected ServiceHost serviceHost = null;

        /// <summary>
        /// Notification handler service host
        /// </summary>
        protected Uri serviceURL = null;


        #endregion

        /// <summary>
        /// Configures this instance.
        /// </summary>
        public void Configure()
        {
            try
            {
                // log4net.ThreadContext.Stacks["NDC"].Push("CONFIG");
                NDC.Push("CONFIG");

                //Load configuration set
                XmlDocument config = new XmlDocument();
                config.Load(ConfigurationManager.AppSettings["ConfigurationSet"]);

                //Create EWS pollers first
                XmlNodeList nodes = config.SelectNodes("/ComponentConfiguration/LangoddsComponents");


                //Then create gatherers
                nodes = config.SelectNodes("//LangoddsComponent");
                logger.InfoFormat("LangoddsComponent job instances found: {0}", nodes.Count);
                foreach (XmlNode nd in nodes)
                {
                    IBaseServiceComponent c = new LangoddsComponent();
                    c.Configure(nd);
                    jobInstances.Add(c.InstanceName, c);
                }

                //Loop over the configured jobs and create a folder set for the push subscription to monitor
                pushSubscriptionJobs.Clear();

            }
            finally
            {
                NDC.Pop();
            }
        }



        public void Pause()
        {
            maintenanceTimer.Stop();
            configFileWatcher.EnableRaisingEvents = false;

            // Stop instances
            foreach (KeyValuePair<String, IBaseServiceComponent> kvp in jobInstances)
            {
                if (kvp.Value.Enabled)
                {
                    kvp.Value.Stop();
                }
            }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            NDC.Push("START");
            try
            {
                //Start instances
                foreach (KeyValuePair<String, IBaseServiceComponent> kvp in jobInstances)
                {
                    if (kvp.Value.Enabled) kvp.Value.Start();
                    logger.Info("Started: " + kvp.Value.InstanceName);
                }


                //Start maintenance
                maintenanceTimer.Start();
            }
            finally
            {
                NDC.Pop();
            }
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            //Kill the notification service handler
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            //Stop instances
            foreach (KeyValuePair<String, IBaseServiceComponent> kvp in jobInstances)
            {
                if (kvp.Value.Enabled) kvp.Value.Stop();
                logger.Info(kvp.Value.InstanceName + " stopped.");
            }
        }

        private void maintenanceTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            MDC.Set("JOBNAME", "MainWorkerJob");
            // logger.DebugFormat("MainServiceComponent::maintenanceTimer_Elapsed() hit");

            try
            {
                maintenanceTimer.Stop();

                /*
                 * I shall check if the jobs that are going really are working and running...
                 * 
                 */
                foreach (KeyValuePair<String, IBaseServiceComponent> kvp in jobInstances)
                {
                    if (kvp.Value.Enabled)
                    {
                        if (kvp.Value.ComponentState == ComponentState.Halted)
                        {
                            logger.Debug("The component is in Halted state, even though it is enabled!");
                            logger.Debug("We are starting the component!");
                            kvp.Value.Start();

                        }
                    }
                }
                maintenanceTimer.Start();

                //Watch the config file
                if (!configFileWatcher.EnableRaisingEvents)
                {
                    configFileWatcher.EnableRaisingEvents = watchConfigSet;
                }

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("MainServiceComponent::maintenanceTimer_Elapsed() failed to renew PushSubscription: " + ex.Message, ex);
            }
        }

        private void configFileWatcher_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            log4net.ThreadContext.Properties["JOBNAME"] = "MainServiceWorker";

            // Outer-try-final to prevent dupe events
            try
            {
                // Stopping file watcher
                configFileWatcher.EnableRaisingEvents = false;
                logger.Info("MainServiceComponent::configFileWatcher_Changed() hit. Will reconfigure.");


                log4net.ThreadContext.Stacks["NDC"].Push("RECONFIGURE");
                // Pause everything
                Pause();

                // Give it a little break
                System.Threading.Thread.Sleep(5000);

                // Reconfigure
                ConfigurationManager.RefreshSection("applicationSettings");
                ConfigurationManager.RefreshSection("appSection");
                Configure();

                Start();


            }
            catch (Exception ex)
            {
                logger.Fatal("NTBNewsFeedService reconfiguration failed - TERMINATING!", ex);
                throw;
            }

            finally
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                configFileWatcher.EnableRaisingEvents = watchConfigSet;
            }
        }


    }
}