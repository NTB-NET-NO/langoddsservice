﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

using log4net;

namespace NTB.Langodds.Utilities
{
    /// <summary>
    /// This class shall do the XSLT-converting. And we are to use only XSLT1.0 because the file has gone through everything else
    /// </summary>
    public class LangoddsConverter
    {
        #region Logging

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog logger = LogManager.GetLogger(typeof(LangoddsConverter));

        #endregion


        #region Directory settings
        public string OutPath { get; set; }
        public string DonePath { get; set; }
        public string ErrorPath { get; set; }
        public string InputPath { get; set; }
        #endregion
        /// <summary>
        /// This is the main constructor
        /// </summary>

        public LangoddsConverter()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!log4net.LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }





        public void GenerateLangodds(string inputname)
        {

            try
            {

                FileInfo fi = new FileInfo(inputname);
                if (!fi.Exists)
                {
                    inputname = this.InputPath + @"\" + inputname;
                }
                // Creating a StreamReader
                logger.Info("Reading: " + this.InputPath + @"\" + inputname);

                StreamReader inn = new StreamReader(inputname, Encoding.GetEncoding("iso-8859-1"));

                fi = new FileInfo(inputname);
                string filename = fi.Name;


                // Creating a DateTime which uses the last write time to the file
                DateTime dt = File.GetLastWriteTime(inputname);

                // Get the current Culture
                CultureInfo ciCurr = CultureInfo.CurrentCulture;

                DateTime dtPassed = DateTime.Now;

                int weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                string weekDay = ciCurr.Calendar.GetDayOfWeek(dtPassed).ToString();

                // Creating a stringarray for the different parts in the file
                string[] parts;

                // This is the content of the line
                string l = "";


                // Creating a StringBuilder string to hold the content of the file before writing it
                StringBuilder strFileContent = new StringBuilder();

                string outfile = "";

                // todo: Add prefix to file
                outfile = Path.Combine(OutPath, Path.GetFileName(inputname).Replace(".txt", ".xml"));

                // Creating new XMLDocument
                XmlDocument doc = new XmlDocument();
                XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
                doc.AppendChild(dec);// Create the root element


                // XmlElement xmlDayTitle = null;

                XmlElement xmlWeek = doc.CreateElement("week");
                XmlElement xmlGames = doc.CreateElement("games");
                XmlElement xmlWeekdays = doc.CreateElement("weekdays");
                XmlElement
                    xmlMainRoot,
                    xmlWeekday,
                    xmlDate,
                    xmlGame,
                    xmlGamenumber,
                    xmlGameteams,
                    xmlGameneutral,
                    xmlGamesfield,
                    xmlGamehomeodds,
                    xmlGameevenodds,
                    xmlGameawayodds,
                    xmlLeagueobject,
                    xmlTvchannel = null;

                XmlText xmlText;

                xmlMainRoot = doc.CreateElement("langodds");
                xmlWeek = doc.CreateElement("week");
                xmlText = doc.CreateTextNode(weekNum.ToString());
                xmlWeek.AppendChild(xmlText);
                xmlMainRoot.AppendChild(xmlWeek);
                string strStoreTime = "";
                string strStorePlayDay = "";
                bool GamesTag = false;
                bool WeekTag = false;

                string str59er = "";

                string strDate = "";
                string strDay = "";

                string strStoreDay = "";

                while (inn.Peek() >= 0)
                {
                    bool DayTag = false;
                    string dayPart = "";
                    // Reads the content of the current line
                    l = inn.ReadLine();
                    logger.Debug("Computing following line: " + l);
                    // Splitting the line with tabs
                    bool convertLine = true;
                    if (l.ToLower().Substring(0, 6) == "mandag")
                    {
                        convertLine = false;
                        DayTag = true;
                    }
                    else if (l.ToLower().Substring(0, 7) == "tirsdag")
                    {
                        convertLine = false;
                        DayTag = true;
                    }
                    else if (l.ToLower().Substring(0, 6) == "onsdag")
                    {
                        convertLine = false;
                        DayTag = true;
                    }
                    else if (l.ToLower().Substring(0, 7) == "torsdag")
                    {
                        convertLine = false;
                        DayTag = true;
                    }
                    else if (l.ToLower().Substring(0, 6) == "fredag")
                    {
                        convertLine = false;
                        DayTag = true;
                    }
                    else if (l.ToLower().Substring(0, 6) == "lørdag")
                    {
                        convertLine = false;
                        DayTag = true;
                    }
                    else if (l.ToLower().Substring(0, 6) == "søndag")
                    {
                        convertLine = false;
                        DayTag = true;
                    }

                    if (convertLine == false)
                    {
                        if (DayTag == true)
                        {
                            // Getting the day part of the string
                            dayPart = "";
                            if (l.Contains("\t"))
                            {
                                dayPart = Regex.Split(l, "\t").GetValue(0).ToString();
                            }
                            else
                            {
                                dayPart = l;
                            }

                            // Get some more data from this line
                            string[] monthPart = Regex.Split(dayPart, " ");

                            int intMonth =
                                DateTime.ParseExact(monthPart[2], "MMMM", CultureInfo.CurrentCulture).Month;
                            int monthDay = Convert.ToInt32(monthPart[1].Replace(".", ""));
                            strDate = monthDay + "/" + intMonth;
                            strDay = monthPart[0];

                            //xmlDayTitleTitle = doc.CreateElement("Title");
                            //xmlText = doc.CreateTextNode(dayPart);
                            //xmlDayTitleTitle.AppendChild(xmlText);
                            //xmlMainRoot.AppendChild(xmlDayTitle);

                            

                            DayTag = false;

                        }
                    }



                    if (convertLine == true)
                    {
                        parts = Regex.Split(l, "\t");
                        /*
                         * Time: 17.25	(or empty)
                         * 1	(Object ID)
                         * Kaiserslautern - Jahn Regensburg		
                         * S	
                         * HomeOdds: 1,25	
                         * EvenOdds: 4,40	
                         * AwayOdds: 6,75	
                         * League: TYS2	
                         * Broadcaster: Eurosport 2
                         */

                        // Cleaning up the different posts in the array
                        // Removing spaces with trim
                        int intParts = parts.Length;

                        logger.Debug("This line contains " + parts.Length.ToString() + " parts");

                        int intCounter = 0;
                        foreach (string part in parts)
                        {
                            string tmp = part;
                            parts[intCounter] = tmp.Trim();
                            intCounter++;
                        }

                        intParts = parts.Length;
                        logger.Debug("After trimming this line contains " + parts.Length.ToString() + " parts");

                        // Checking if the split results in less than 10 posts
                        if (intParts < 10)
                        {

                            // Doing some initial checks
                            if (intParts < 8)
                            {
                                logger.Info("We are now adding tabulators to get to eight parts");
                                logger.Info("This line contains: " + intParts + " parts!");
                                int intEightParts = 8;
                                int intPartsLeft = intEightParts - intParts - 1;
                                string strPartsAdding = "";

                                while (intPartsLeft < intEightParts)
                                {
                                    strPartsAdding += "\t";
                                    intPartsLeft++;
                                }



                                string temp = parts[intParts - 1];

                                // adding a tabulator to this line
                                parts[intParts - 1] = temp + strPartsAdding;

                                string newline = String.Join("\t", parts);

                                logger.Info("This string is now like this: " + newline);

                                // This splits the line.
                                parts = Regex.Split(newline, "\t");

                                logger.Info("After checking for line 8 this line NOW contains " +
                                            parts.Length + " parts");
                            }

                            // Checking if parts[8] starts with R

                            logger.Info("We are checking parts[7]: '" + parts[7] + "'");

                            // This code is no longer in used
                            if (parts[7].StartsWith("R"))
                            {

                                logger.Info("We are going to add a tab in front of the R");

                                string temp = parts[7].ToString();
                                parts[7] = "\t" + temp;

                                string newline = String.Join("\t", parts);

                                logger.Info("This string is now like this: " + newline);

                                parts = Regex.Split(newline, "\t");

                                logger.Info("After checking for a R-line, this line NOW contains " +
                                            parts.Length.ToString() + " parts");
                            }



                            // Checking if we now have 13 array posts
                            if (parts.Length < 10)
                            {

                                int intAddingTabs = 0;
                                int intMissingTabs = 10 - parts.Length;

                                while (intAddingTabs < intMissingTabs)
                                {
                                    parts[5] += "\t";
                                    intAddingTabs++;
                                }

                                string newline = String.Join("\t", parts);

                                parts = Regex.Split(newline, "\t");

                                logger.Info("After adding " + intMissingTabs.ToString() + " this line now contains " +
                                            parts.Length.ToString() + " parts");


                                // if it's still less than 13 we'll add tabs at the end
                                if (parts.Length < 10)
                                {
                                    // Setting max and min
                                    int max = 9;
                                    int min = parts.Length;
                                    int debugmin = max - min;
                                    string strTabs = "";

                                    // Looping until min eq max
                                    while (min <= max)
                                    {
                                        // Add tabs to the strTabs string
                                        strTabs += "\t";
                                        min++;
                                    }

                                    // Add tabs to the end of the newline string
                                    newline += strTabs;

                                    // Split line based on tabs. Now we shall have 12/13 arrays
                                    parts = Regex.Split(newline, "\t");

                                    logger.Info("After adding " + debugmin + " this line NOW contains " +
                                                parts.Length.ToString() + " parts");

                                }
                            }
                        }
                        logger.Info("Done cleaning parts");



                        string strTime = parts[0];
                        string strMatchNumber = parts[1];
                        string strMatch = parts[2];
                        /* 
                         * Removing & if we have such a character, 
                         * but only if we have it between spaces. 
                         * This so that we don't get:
                         * &amp; -> &amp;amp;amp;.....
                         */
                        strMatch = strMatch.Replace(" & ", " &amp; ");
                        string strNeutralField = parts[3];
                        string strSField = parts[4];
                        string strHomeOdds = parts[5];
                        string strEvenOdds = parts[6];
                        string strAwayOdds = parts[7];
                        string strLeagueObject = parts[8];

                        string strTvChannel = parts[9];


                        if (strTime != "")
                        {
                            if (WeekTag == true)
                            {
                                WeekTag = false;
                                GamesTag = false;
                            }
                            
                            xmlWeekdays = doc.CreateElement("weekdays");
                            xmlWeekday = doc.CreateElement("weekday");
                            xmlText = doc.CreateTextNode(strDay.Replace(".",""));
                            xmlWeekday.AppendChild(xmlText);

                            xmlDate = doc.CreateElement("date");
                            xmlText = doc.CreateTextNode(strDate);
                            xmlDate.AppendChild(xmlText);

                            xmlWeekdays.AppendChild(xmlWeekday);
                            xmlWeekdays.AppendChild(xmlDate);
                            


                            xmlGames = doc.CreateElement("games");
                            xmlGames.SetAttribute("time", strTime);
                            xmlGames.SetAttribute("date", strDate);
                            xmlGames.SetAttribute("day", strDay.Replace(".",""));
                            xmlWeekdays.AppendChild(xmlGames);
                            xmlWeek.AppendChild(xmlWeekdays);
                            WeekTag = true;
                        }


                        xmlGame = doc.CreateElement("game");

                        xmlGamenumber = doc.CreateElement("gamenumber");
                        xmlText = doc.CreateTextNode(strMatchNumber);
                        xmlGamenumber.AppendChild(xmlText);
                        xmlGame.AppendChild(xmlGamenumber);

                        xmlGameteams = doc.CreateElement("gameteams");
                        xmlText = doc.CreateTextNode(strMatch);
                        xmlGameteams.AppendChild(xmlText);
                        xmlGame.AppendChild(xmlGameteams);

                        xmlGameneutral = doc.CreateElement("gameneutral");
                        xmlText = doc.CreateTextNode(strNeutralField);
                        xmlGameneutral.AppendChild(xmlText);
                        xmlGame.AppendChild(xmlGameneutral);

                        xmlGamesfield = doc.CreateElement("gamesfield");
                        xmlText = doc.CreateTextNode(strSField);
                        xmlGamesfield.AppendChild(xmlText);
                        xmlGame.AppendChild(xmlGamesfield);

                        xmlGamehomeodds = doc.CreateElement("gamehomeodds");
                        xmlText = doc.CreateTextNode(strHomeOdds);
                        xmlGamehomeodds.AppendChild(xmlText);
                        xmlGame.AppendChild(xmlGamehomeodds);

                        xmlGameevenodds = doc.CreateElement("gameevenodds");
                        xmlText = doc.CreateTextNode(strEvenOdds);
                        xmlGameevenodds.AppendChild(xmlText);
                        xmlGame.AppendChild(xmlGameevenodds);

                        xmlGameawayodds = doc.CreateElement("gameawayodds");
                        xmlText = doc.CreateTextNode(strAwayOdds);
                        xmlGameawayodds.AppendChild(xmlText);
                        xmlGame.AppendChild(xmlGameawayodds);

                        xmlLeagueobject = doc.CreateElement("leagueobject");
                        xmlText = doc.CreateTextNode(strLeagueObject);
                        xmlLeagueobject.AppendChild(xmlText);
                        xmlGame.AppendChild(xmlLeagueobject);

                        xmlTvchannel = doc.CreateElement("tvchannel");
                        xmlText = doc.CreateTextNode(strTvChannel);
                        xmlTvchannel.AppendChild(xmlText);
                        xmlGame.AppendChild(xmlTvchannel);
                        // Creating game number 

                        xmlGames.AppendChild(xmlGame);
                        xmlWeekdays.AppendChild(xmlGames);
                        
                    }
                    if (!xmlWeekdays.IsEmpty)
                    {
                        xmlMainRoot.AppendChild(xmlWeekdays);
                    }

                }
                doc.AppendChild(xmlMainRoot);

                // Closing the readfile
                inn.Dispose();


                // Creating the filename for the file. Removing the .txt-extention and replacing it with .xml 
                // string completeFilename = @"c:\" + DocumentName;
                XmlWriterSettings writerSettings = new XmlWriterSettings
                    {
                        Indent = true,
                        OmitXmlDeclaration = false,
                        Encoding = Encoding.UTF8
                    };

                // TODO: Change Using to While so that we can loop in case the file is in use...

                using (XmlWriter writer = XmlWriter.Create(outfile, writerSettings))
                {
                    try
                    {
                        doc.Save(writer);
                    }
                    catch (IOException ioex)
                    {
                        logger.Debug(ioex);
                    }
                }


                logger.Info("--------------------------------------------------------");

                logger.Info("moving file to " + DonePath + @"\" + filename);


                FileInfo fif = new FileInfo(DonePath + @"\" + filename);
                if (fif.Exists)
                {
                    fif.Delete();
                    fi.MoveTo(DonePath + @"\" + filename);
                }
                else
                {

                    fi.MoveTo(DonePath + @"\" + filename);
                }


            }
            catch (IOException iox)
            {
                logger.Info(iox.Message);

                logger.Info(iox.StackTrace);

            }
            catch (IndexOutOfRangeException iex)
            {
                logger.Info(iex.Message);

                logger.Info(iex.StackTrace);
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);

                logger.Info(ex.StackTrace);
            }
        }

    }
}