﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Langodds.Utilities
{
    public class LangoddsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public LangoddsException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="exception">INternal exception.</param>
        public LangoddsException(string message, Exception exception)
            : base(message, exception)
        { }

    }
}

