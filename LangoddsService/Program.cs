﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using ntb_FuncLib;

namespace LangoddsService
{
    public class Program
    {
        public string dirLogPath = Properties.Settings.Default.dirLogPath;
        public string dirRootPath = Properties.Settings.Default.dirRootPath;
        public string dirErrorPath = Properties.Settings.Default.dirErrorPath;

        public string strMessage = "";



        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new LangoddsService() 
			};
            ServiceBase.Run(ServicesToRun);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strFolder"></param>
        public void LoopDirectories(string strFolder)
        {

            // Creating the string Path
            string strPath = "";
            if (!strFolder.StartsWith("c:")) {
                strPath = Properties.Settings.Default.dirInputPath + @"\" + strFolder;
            } else {
                strPath = strFolder;
            }
            strMessage = "Dette er strPath: " + strPath;
            LogFile.WriteLog(ref dirLogPath, ref strMessage);
            // Get the folder

            LoopFiles(strPath);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strPath"></param>
        public void LoopDirectory(string strPath)
        {
            // Looping through the directory

            string[] directories = Directory.GetDirectories(strPath);
            foreach (string strDir in directories)
            {
                string strDirectory = strDir.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strDirectory);

                // DoConvert(strDir);
                // GenerateLangodds(string inputname)
                LoopDirectory(strDir);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDirectory"></param>
        public void LoopFiles(string strDirectory)
        {
            string[] directory = Directory.GetFiles(strDirectory);

            foreach (string strFiles in directory)
            {
                strMessage = "Converting: " + strFiles;
                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                CreateDocument cd = new CreateDocument();
                cd.GenerateLangodds(strFiles);
            }
        }

    }
}
