﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using ntb_FuncLib;

namespace LangoddsService
{
    public partial class LangoddsService : ServiceBase
    {

        string dirLogPath = Properties.Settings.Default.dirLogPath;
        string dirRootPath = Properties.Settings.Default.dirRootPath;
        string dirErrorPath = Properties.Settings.Default.dirErrorPath;

        private static LogFile logFile = new LogFile();

        bool boolRunningConvert = false;
        public LangoddsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Folder f = new Folder();
                
                string strLine = "------------------------------------------------------------";
                string strStartMessage = "                  STARTING Langodds Service";
                LogFile.WriteLogNoDate(ref dirLogPath, ref strLine);
                LogFile.WriteLogNoDate(ref dirLogPath, ref strStartMessage);
                LogFile.WriteLogNoDate(ref dirLogPath, ref strLine);

                IntervalTimer.Interval = Convert.ToInt32(Properties.Settings.Default.PollingInterval);
                IntervalTimer.Enabled = true;
                IntervalTimer.Start();
            }
            catch (Exception e)
            {
                string strMessage = e.Message.ToString();
                string strStack = e.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                LogFile.WriteLog(ref dirLogPath, ref strStack);


                if (e.InnerException.Message != "")
                {
                    strMessage = e.InnerException.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }
            }
        }

        protected override void OnStop()
        {
            try
            {
                string strLine = "------------------------------------------------------------";
                string strStartMessage = "                  STOPPING NTB LangoddsService";
                LogFile.WriteLogNoDate(ref dirLogPath, ref strLine);
                LogFile.WriteLogNoDate(ref dirLogPath, ref strStartMessage);
                LogFile.WriteLogNoDate(ref dirLogPath, ref strLine);

                // TODO: Add code here to perform any tear-down necessary to stop your service
                IntervalTimer.Enabled = false;
                IntervalTimer.Stop();
            }
            catch (Exception e)
            {
                string strMessage = e.Message.ToString();
                string strStack = e.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                LogFile.WriteLog(ref dirLogPath, ref strStack);


                if (e.InnerException.Message != "")
                {
                    strMessage = e.InnerException.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }

            }
        }

        private void IntervalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string strMessage = "In Elapsed timer!";
                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                // TODO: Create code here to run when the service has started
                if (boolRunningConvert == false)
                {
                    strMessage = "sett to true";
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    boolRunningConvert = true;
                    Program p = new Program();

                    string strInputPath = Properties.Settings.Default.dirInputPath;
                    p.LoopDirectories(strInputPath);

                    strMessage = "sett to false";
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                    boolRunningConvert = false;
                }
            }
            catch (Exception ie)
            {
                string strMessage = "An error has occured: ";
                strMessage += ie.Message.ToString();

                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                boolRunningConvert = false;
            }
        }
    }
}
